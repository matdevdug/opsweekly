<?php
include_once("phplib/base.php");

$time_requested = getOrSetRequestedDate();

$my_username = getUsername();

$start_end = getWeekRange($time_requested);
$start_ts = $start_end[0];
$end_ts = $start_end[1];

$oncall_period = getOnCallWeekRange($time_requested);
$oncall_start = $oncall_period[0];
$oncall_end = $oncall_period[1];


$page_title = getTeamName() . " Weekly Updates - Add new update";
include_once('phplib/header.php');
include_once('phplib/nav.php')

?>

<div class="container">
<h1>Update for week ending <?php echo date("l jS F Y", $end_ts ) ?></h2>
<div class="row">
    <div class="span12">
        <?php 
        if (getTeamConfig('oncall')) {
        ?>
        <h2>On call report</h2>
        <?php if(isset($_GET['oncall_succ'])) {
            echo insertNotify("success", "Your on call update has been saved successfully");
        }
        ?>
        <div id="on-call-question">
        <?php 
        // See if a report was already submitted for this week. Doesn't matter, just a heads up. 
        if ($oncall_user = guessPersonOnCall($oncall_start, $oncall_end)) {
            if ($oncall_user == $my_username) {
                echo "<p>You have already submitted a report this week, but you can update or add to it by clicking the button below</p>";
            } else {
                echo "<p>An on call report has already been submitted by someone else this week, but you can update or add to it by clicking the button below</p>";
            }
        } else {
            echo "<p>Were you on call this week? Click button to load notification report</p>";
        }
        ?>
        <button type="button" class="btn btn-danger" data-loading-text="Generating On Call Summary..." 
            onclick="$(this).button('loading'); $('.notifications').load('generate_oncall_survey.php?date=<?php echo urlencode($time_requested) ?>', function() { $('#on-call-question').fadeOut('fast') });">I was on call</button>
        </div>
        <div class="notifications" id="notifications"></div>
        <p></p><br />
        <?php }  ?>
            <div class="span5">
            <?php
                // This is where hints aka weekly providers are loaded and displayed
                printWeeklyHints($my_username, $start_ts, $end_ts);
            ?>
            </div>
        </div>
  
    </div>
</div>
</form>

</div>
<?php include_once('phplib/footer.php'); ?>
</body>
</html>
